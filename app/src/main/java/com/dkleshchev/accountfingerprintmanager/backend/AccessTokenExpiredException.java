package com.dkleshchev.accountfingerprintmanager.backend;

public class AccessTokenExpiredException extends Exception {
    @Override
    public String getMessage() {
        return "Access token is expired!";
    }
}
