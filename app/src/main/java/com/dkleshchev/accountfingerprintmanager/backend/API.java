package com.dkleshchev.accountfingerprintmanager.backend;

public interface API {
    Response login(final String userName, final String password);
}
