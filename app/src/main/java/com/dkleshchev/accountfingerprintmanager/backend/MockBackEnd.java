package com.dkleshchev.accountfingerprintmanager.backend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.security.SecureRandom;

public class MockBackEnd implements API {
    private static final String TAG = MockBackEnd.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(TAG);

    @Override
    public Response login(String userName, String password) {
        return new Response(new User(userName, password, generateToken(userName, password)), null, null);
    }

    private String generateToken(final String username,
                                 final String password) {
        SecureRandom random = new SecureRandom();
        final String result = new BigInteger(username.length() * password.length(), random).toString(32);
        LOGGER.debug("generateToken: {}", result);
        return result;
    }
}
