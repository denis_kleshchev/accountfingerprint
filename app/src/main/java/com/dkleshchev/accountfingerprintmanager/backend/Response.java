package com.dkleshchev.accountfingerprintmanager.backend;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(suppressConstructorProperties = true)
public class Response {
    private final User user;
    private final String data;
    private final Exception exception;
}
