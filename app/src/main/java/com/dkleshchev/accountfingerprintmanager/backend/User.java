package com.dkleshchev.accountfingerprintmanager.backend;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(suppressConstructorProperties = true)
public class User {
    private final String userName;
    private final String password;
    private final String accessToken;
}
