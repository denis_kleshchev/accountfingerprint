package com.dkleshchev.accountfingerprintmanager.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dkleshchev.accountfingerprintmanager.R;
import com.dkleshchev.accountfingerprintmanager.manager.FingerprintHelper;
import com.dkleshchev.accountfingerprintmanager.util.CryptoUtils;
import com.dkleshchev.accountfingerprintmanager.util.FingerprintUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;

public class FingerActivity extends AppCompatActivity {
    private SharedPreferences preferences;
    private FingerprintHelper fingerprintHelper;
    private static final String TAG = FingerActivity.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(TAG);

    @BindView(R.id.edit_query)
    EditText editText;

    public static Intent createIntent(Context context) {
        return new Intent(context, FingerActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        LOGGER.debug("onCreate()");

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LOGGER.debug("fab pressed()");
                prepareLogin();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LOGGER.debug("onResume()");
        if (preferences.contains(FingerprintHelper.PIN)) {
            LOGGER.debug("onResume(), preferences contains pin");
            prepareSensor();
        } else {
            LOGGER.debug("onResume(), preferences DOES NOT contain pin");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        LOGGER.debug("onStop()");
        if (fingerprintHelper != null) {
            LOGGER.debug("onStop(), fingerprintHelper != null");
            fingerprintHelper.cancel();
        }
    }

    private void prepareLogin() {
        LOGGER.debug("prepareLogin()");
        final String pin = editText.getText().toString();
        if (pin.length() > 0) {
            LOGGER.debug("pin correct!");
            savePin(pin);
            setResult(RESULT_OK);
            finish();
        } else {
            Toast.makeText(this, "pin is empty", Toast.LENGTH_SHORT).show();
        }
    }

    private void savePin(String pin) {
        LOGGER.debug("savePin(), sensor state: {}", FingerprintUtils.checkSensorState(this));
        if (FingerprintUtils.isSensorStateAt(FingerprintUtils.SensorState.READY, this)) {
            String encoded = CryptoUtils.encode(pin);
            preferences.edit().putString(FingerprintHelper.PIN, encoded).apply();
        }
    }

    private void prepareSensor() {
        LOGGER.debug("prepareSensor(), sensor state: {}", FingerprintUtils.checkSensorState(this));
        if (FingerprintUtils.isSensorStateAt(FingerprintUtils.SensorState.READY, this)) {
            FingerprintManagerCompat.CryptoObject cryptoObject = CryptoUtils.getCryptoObject();
            if (cryptoObject != null) {
                LOGGER.debug("prepareSensor(), cryptoObject != null");
                Toast.makeText(this, "use fingerprint to login", Toast.LENGTH_LONG).show();
                fingerprintHelper = new FingerprintHelper(this, preferences, new Action1<String>() {
                    @Override
                    public void call(String s) {
                        editText.setText(s);
                    }
                });
                fingerprintHelper.startAuth(cryptoObject);
            } else {
                LOGGER.debug("prepareSensor(), cryptoObject == null");
                preferences.edit().remove(FingerprintHelper.PIN).apply();
                Toast.makeText(this, "new fingerprint enrolled. enter pin again", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
