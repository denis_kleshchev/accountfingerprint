package com.dkleshchev.accountfingerprintmanager.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.dkleshchev.accountfingerprintmanager.R;
import com.dkleshchev.accountfingerprintmanager.backend.API;
import com.dkleshchev.accountfingerprintmanager.backend.MockBackEnd;
import com.dkleshchev.accountfingerprintmanager.backend.Response;
import com.dkleshchev.accountfingerprintmanager.manager.MyAuthenticator;
import com.dkleshchev.accountfingerprintmanager.util.PermissionsUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action0;
import rx.functions.Action1;

public class MainActivity extends AppCompatActivity {
    protected static final int PERMISSIONS_REQUEST = 1;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(TAG);
    private AccountManager accountManager;
    private API api;

    private PermissionsUtil.PermissionCallback permissionCallback;

    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountManager = AccountManager.get(this);
        api = new MockBackEnd();
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        LOGGER.debug("onCreate()");
    }

    @OnClick(R.id.add_account)
    public void onAddAccountClick() {
        LOGGER.debug("Add click");
        startActivity(LoginActivity.createIntent(this));
    }

    @OnClick(R.id.get_token)
    public void onGetTokenClick() {
        checkPermissionsAndExecute(getAccounts(getToken()), PermissionsUtil.PermissionGroup.ACCOUNTS);
    }

    @OnClick(R.id.invalidate)
    public void onInvalidateClick() {
        checkPermissionsAndExecute(getAccounts(invalidateToken()), PermissionsUtil.PermissionGroup.ACCOUNTS);
    }

    @OnClick(R.id.to_finger)
    public void onToFingerClick() {
        startActivity(FingerActivity.createIntent(this));
    }

    public void checkPermissionsAndExecute(final Action0 actionToExecute, PermissionsUtil.PermissionGroup... permissionGroups) {
        permissionCallback = PermissionsUtil.checkAndRequestPermissions(this, PERMISSIONS_REQUEST, actionToExecute, permissionGroups);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissionCallback != null) {
            permissionCallback.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        }
    }

    private Action0 getAccounts(final Action1<Account> action) {
        return new Action0() {
            @Override
            public void call() {
                if (accountManager != null) {
                    final Account[] accountsByType = accountManager.getAccounts();
                    for (Account account : accountsByType) {
                        if (action != null) {
                            action.call(account);
                        }
                    }
                }
            }
        };
    }

    private Action1<Account> getToken() {
        return new Action1<Account>() {
            @Override
            public void call(final Account account) {
                if (TextUtils.equals(MyAuthenticator.ACCOUNT_TYPE, account.type)) {
                    final String peekAuthToken = accountManager.peekAuthToken(account, account.type);
                    LOGGER.debug("Peeking for account{} auth token: {}", account.name, peekAuthToken);
                    if (peekAuthToken == null) {
                        accountManager.getAuthToken(account, MyAuthenticator.AUTH_TOKEN_TYPE, null, MainActivity.this, new AccountManagerCallback<Bundle>() {
                            @Override
                            public void run(AccountManagerFuture<Bundle> accountManagerFuture) {
                                String result = null;
                                try {
                                    result = accountManagerFuture.getResult().getString(AccountManager.KEY_AUTHTOKEN);
                                    accountManager.setAuthToken(account, MyAuthenticator.ACCOUNT_TYPE, result);
                                } catch (OperationCanceledException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                } catch (AuthenticatorException e) {
                                    e.printStackTrace();
                                }
                                LOGGER.debug("Getting for account{} auth token: {}, main thread? {}", account.name, result, Looper.myLooper() == getMainLooper());
                            }
                        }, null);
                    }
                }
            }
        };
    }

    private Action1<Account> invalidateToken() {
        return new Action1<Account>() {
            @Override
            public void call(Account account) {
                if (TextUtils.equals(MyAuthenticator.ACCOUNT_TYPE, account.type)) {
                    accountManager.invalidateAuthToken(account.type, accountManager.peekAuthToken(account, account.type));
                }
            }
        };
    }
}
