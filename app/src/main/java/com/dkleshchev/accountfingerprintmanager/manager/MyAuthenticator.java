package com.dkleshchev.accountfingerprintmanager.manager;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.dkleshchev.accountfingerprintmanager.activity.LoginActivity;
import com.dkleshchev.accountfingerprintmanager.backend.API;
import com.dkleshchev.accountfingerprintmanager.backend.MockBackEnd;
import com.dkleshchev.accountfingerprintmanager.backend.Response;


public class MyAuthenticator extends AbstractAccountAuthenticator {
    public static final String ACCOUNT_TYPE = "com.dkleshchev.accountfingerprintmanager";
    public static final String IS_ADDING_NEW_ACCOUNT = "is_adding_a_new_account";
    public static final String AUTH_TOKEN_TYPE = "my test access token";

    private final Context context;
    private final API api;

    public MyAuthenticator(Context context) {
        super(context);
        this.context = context;
        api = new MockBackEnd();
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse accountAuthenticatorResponse,
                             String accountType,
                             String authTokenType,
                             String[] requiredFeatures,
                             Bundle options) throws NetworkErrorException {
        final Intent intent = LoginActivity.createIntent(context);
        //account type, auth type, is adding new account = true to be put into intent
        intent.putExtra(IS_ADDING_NEW_ACCOUNT, true);
//        intent.putExtra()
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, accountAuthenticatorResponse);
        final Bundle bundle = new Bundle();
        if (options != null) {
            bundle.putAll(options);
        }
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse accountAuthenticatorResponse, String s) {
        return null;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, Bundle bundle) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse accountAuthenticatorResponse,
                               Account account,
                               String authTokenType,
                               Bundle options) throws NetworkErrorException {
        final AccountManager accountManager = AccountManager.get(context);

        String token = accountManager.peekAuthToken(account, authTokenType);

        if (TextUtils.isEmpty(token)) {
            final String password = accountManager.getPassword(account);
            if (!TextUtils.isEmpty(password)) {
                final Response response = api.login(account.name, password);
                token = response.getUser().getAccessToken();
            }
        }

        if (!TextUtils.isEmpty(token)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, token);
            return result;
        }

        final Intent intent = LoginActivity.createIntent(context);
        //account type, auth type to be put into intent
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, ACCOUNT_TYPE);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, accountAuthenticatorResponse);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public String getAuthTokenLabel(String s) {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String s, Bundle bundle) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse accountAuthenticatorResponse, Account account, String[] strings) throws NetworkErrorException {
        return null;
    }
}
