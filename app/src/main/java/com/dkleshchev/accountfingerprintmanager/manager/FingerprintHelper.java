package com.dkleshchev.accountfingerprintmanager.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompat;
import android.support.v4.os.CancellationSignal;
import android.widget.Toast;

import com.dkleshchev.accountfingerprintmanager.util.CryptoUtils;

import javax.crypto.Cipher;

import rx.functions.Action1;

public class FingerprintHelper extends FingerprintManagerCompat.AuthenticationCallback {
    public static final String PIN = "pin";
    private final Context context;
    private final SharedPreferences preferences;
    private CancellationSignal cancellationSignal;
    private Action1<String> callback = null;

    public FingerprintHelper(Context context, SharedPreferences preferences, final Action1<String> callback) {
        this.context = context;
        this.preferences = preferences;
        this.callback = callback;
    }

    public void startAuth(final FingerprintManagerCompat.CryptoObject cryptoObject) {
        cancellationSignal = new CancellationSignal();
        final FingerprintManagerCompat manager = FingerprintManagerCompat.from(context);
        manager.authenticate(cryptoObject, 0, cancellationSignal, this, null);
    }

    public void cancel() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
        }
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        Toast.makeText(context, errString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        Toast.makeText(context, helpString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(context, "Fail! Try again", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        final Cipher cipher = result.getCryptoObject().getCipher();
        final String encoded = preferences.getString(PIN, null);
        final String decoded = CryptoUtils.decode(encoded, cipher);
        Toast.makeText(context, "success:: " + decoded, Toast.LENGTH_SHORT).show();
        if (callback != null) {
            callback.call(decoded);
        }
    }
}
