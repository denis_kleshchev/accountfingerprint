package com.dkleshchev.accountfingerprintmanager.util;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.functions.Action0;

public final class PermissionsUtil {

    private static final String TAG = PermissionsUtil.class.getSimpleName();
    private static final Logger LOGGER = LoggerFactory.getLogger(TAG);

    private PermissionsUtil() {
        throw new UnsupportedOperationException("Can't instantiate PermissionUtil");
    }

    public static PermissionCallback checkAndRequestPermissions(final Activity activity, final int requestCode, final Action0 action, final PermissionGroup... groups) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            action.call();
            return null;
        }

        final List<String> permissions = new ArrayList<>();
        for (PermissionGroup permissionGroup : groups) {
            permissions.addAll(Arrays.asList(permissionGroup.getPermissions()));
        }
        for (int i = 0; i < permissions.size(); ) {
            if (ActivityCompat.checkSelfPermission(activity, permissions.get(i))
                    == PackageManager.PERMISSION_GRANTED) {
                permissions.set(i, permissions.get(permissions.size() - 1));
                permissions.remove(permissions.get(permissions.size() - 1));
            } else {
                i++;
            }
        }
        if (permissions.size() == 0) {
            action.call();
            return null;
        } else {
            ActivityCompat.requestPermissions(activity,
                    permissions.toArray(new String[permissions.size()]), requestCode);

            return new PermissionCallback(requestCode, Arrays.asList(groups), action);
        }
    }

    public enum PermissionGroup {
        STORAGE(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}),
        LOCATION(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}),
        ACCOUNTS(new String[]{Manifest.permission.GET_ACCOUNTS});

        private final String[] permissions;

        PermissionGroup(String[] permissions) {
            this.permissions = permissions;
        }

        public String[] getPermissions() {
            return permissions;
        }

    }

    public static class PermissionCallback {
        private final int code;
        private final List<PermissionGroup> groups;
        private Action0 action;

        private PermissionCallback(int requestCode, List<PermissionGroup> group, Action0 action) {
            code = requestCode;
            this.groups = group;
            this.action = action;
        }

        public void onRequestPermissionsResult(Activity activity, int requestCode, String permissions[], int[] grantResults) {
            if (requestCode != code) {
                return;
            }
            boolean granted = true;
            for (int grantResult : grantResults) {
                granted = granted && (grantResult == PackageManager.PERMISSION_GRANTED);
            }
            if (granted) {
                LOGGER.debug("{} permissions granted", groups);
                if (action != null) {
                    action.call();
                }
            } else {
                LOGGER.debug("{} permissions denied", groups);
            }
        }

        public PermissionCallback callOnSuccess(final Action0 action) {
            this.action = action;
            return this;
        }
    }
}
